﻿using System.Collections.Generic;

namespace EF_Charniauski.DAL.Interfaces
{
    public interface IRepository<TEntity> where TEntity: IEntity
    {
        void Add(TEntity entity);

        void Delete(TEntity entity);

        IEnumerable<TEntity> GetAll();
    }
}
