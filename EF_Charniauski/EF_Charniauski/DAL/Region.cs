﻿using System.Collections.Generic;
using EF_Charniauski.DAL.Interfaces;

namespace EF_Charniauski.DAL
{
    public class Region : IEntity
    {
        public int Id { get; set; }
        public string RegionDescription { get; set; }

        public virtual List<Region> Regions { get; set; }
    }
}
