﻿using EF_Charniauski.DAL.Interfaces;

namespace EF_Charniauski.DAL
{
    public class Territory : IEntity
    {
        public int Id { get; set; }
        public string TerritoryDescription { get; set; }
        public int RegionId { get; set; }

        public virtual Region Region { get; set; }
    }
}
