﻿using System;
using System.Collections.Generic;
using System.Linq;
using EF_Charniauski.DAL;
using EF_Charniauski.DAL.Repositories;

namespace EF_Charniauski
{
    class Program
    {
        static void Main(string[] args)
        {
            OrderRepository orderRepository = new OrderRepository();
            IEnumerable<Order> orders = orderRepository.GetAll().ToList();

            foreach (var order in orders)
            {
                Console.WriteLine(order.OrderId);
                Console.WriteLine(order.Customer.CompanyName);
            }

            Console.ReadLine();
        }
    }
}
