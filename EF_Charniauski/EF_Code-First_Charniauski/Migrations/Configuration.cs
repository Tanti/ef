using System.Data.Entity.Migrations;

namespace EF_Code_First_Charniauski.Migrations
{

    internal sealed class Configuration : DbMigrationsConfiguration<EF_Code_First_Charniauski.Northwind>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(EF_Code_First_Charniauski.Northwind context)
        {
            context.Categories.AddOrUpdate(
              new Categories() { CategoryName = "pew" },
              new Categories { CategoryName = "pow" },
              new Categories { CategoryName = "puw" }
            );

            context.Regions.AddOrUpdate(
                new Regions() { RegionDescription = "Mikitka"}
                );

            context.Territories.AddOrUpdate(
                new Territories() { RegionID = 2 , TerritoryDescription = "asd"}
                );
        }
    }
}
